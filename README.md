A CRF sequence model to predict turn labels based on the few labeled files we've go so far. The test script (driver.py) performs cross-validation. 

The average F1 from all 10 tests is 66.7%.
