import os
import csv
import argparse
from pprint import pprint
import sklearn_crfsuite
from sklearn_crfsuite import metrics
from sklearn.model_selection import cross_val_score

from data_reader import Conversation

parser = argparse.ArgumentParser(description='Train a CRF model on turn labels and perform cross-validation.')
parser.add_argument("data_path",
                    help="path to the directory containging .xml files with parsed conversations")
parser.add_argument("-s", "--split_size",
                    help="percent (as a decimal) of data to set aside for training",
                    type=float,
                    default=0.2)
parser.add_argument("-n", "--num_splits",
                    help="number of cross-validation cycles. default is 10",
                    type=int,
                    default=10)

args = parser.parse_args()
vocab = []
labelled_convos = []
for root, dirs, files in os.walk(args.data_path):
    for file in files:
        if "conversation" in file:
            convo = Conversation(os.path.join(root, file))
            if convo.feature_list:
                labelled_convos.append(convo)


f1s = []

amount_to_move = int(len(labelled_convos)/args.num_splits)
test_start = 0
test_end = int(args.split_size*len(labelled_convos))

for i in range(args.num_splits):

    num_labelled = len(labelled_convos)
    X_train = [convo.feature_list for i, convo in enumerate(labelled_convos) if i < test_start or i > test_end]
    y_train = [convo.label_list for i, convo in enumerate(labelled_convos) if i < test_start or i > test_end]

    X_test = [convo.feature_list for convo in labelled_convos[test_start:test_end]]
    y_test = [convo.label_list for convo in labelled_convos[test_start:test_end]]

    crf = sklearn_crfsuite.CRF(
        algorithm='lbfgs',
        c1=0.1,
        c2=0.1,
        max_iterations=100,
        all_possible_transitions=True
    )
    crf.fit(X_train, y_train)

    y_pred = crf.predict(X_test)
    f1 = metrics.flat_f1_score(y_test, y_pred, average='weighted')
    f1s.append(f1)

    with open("results/run_{}.out".format(str(i)), "w+") as output_file:
        # print error report to file
        sorted_labels = sorted(crf.classes_)
        output_file.write(metrics.flat_classification_report(y_test, y_pred, labels=sorted_labels, digits=3))

    test_start += amount_to_move
    test_end += amount_to_move

print("average F1: ", sum(f1s)/len(f1s))
