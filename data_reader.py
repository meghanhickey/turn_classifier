import os
import re
import xml.etree.ElementTree as ET
from pprint import pprint


class Conversation(object):
    number_regex = re.compile(r"[0-9]{5}")

    def __init__(self, file_name):
        self.file_name = file_name
        self.id = re.search(self.number_regex, file_name).group(0)
        self.tree = ET.parse(file_name)
        self.root = self.tree.getroot()
        self.turn_tree_list = [tree for tree in self.tree.iter("turn")]
        self.turns = [Turn(self, tree, i) for i, tree in enumerate(self.tree.iter("turn"))]
        self.labels = [turn.label for turn in self.turns if turn.label != ""]

        if len(self.labels) > 13:
            self.feature_list = [turn.features for turn in self.turns[13:]]
            self.label_list = self.labels[13:]
            if len(self.feature_list) != len(self.label_list):
                print(self.file_name)
            # assert len(self.feature_list) == len(self.label_list)

        else:
            self.feature_list = None
            self.label_list = None


class Turn(object):

    def __init__(self, convo_tree, turn_tree, index):
        self.convo_tree = convo_tree
        self.turn_tree = turn_tree
        self.index = index
        self.label = self.turn_tree.attrib["label"]
        self.utterances = [elem.strip() for elem in self.turn_tree.itertext() if elem.strip() != ""]
        self.words = [word.lower() for utter in self.utterances for word in utter.split()]
        self.features = self.get_features()

    def get_features(self):
        feats = dict()

        feats["bias"] = 1.0
        feats["prev_label"] = self.convo_tree.turn_tree_list[self.index - 1].attrib["label"] if self.index > 0 else -1
        feats["num_utter"] = len(self.utterances)
        feats["num_words"] = len(self.words)
        vocab = set(self.words)
        # feats["vocab_size"] = len(vocab)
        # feats["clinic"] = True if "clinic" in vocab else False
        feats["next_label"] = self.convo_tree.turn_tree_list[self.index + 1].attrib["label"] \
            if self.index < len(self.convo_tree.turn_tree_list) - 1 \
            else -1

        return feats
